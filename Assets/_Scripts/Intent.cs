﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intent : MonoBehaviour {

    static bool IsInitialized = false;
    
    static void Initialize()
    {
        if (IsInitialized)
            return;
        IsInitialized = true;
    }
    
	public static void IntentShareText(string text)
    {
        if (Application.platform != RuntimePlatform.Android)
            return;

        #if UNITY_ANDROID

        if (!IsInitialized)
            Initialize();

        #endif

    }
}
