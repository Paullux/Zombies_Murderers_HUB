﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RotateCameraScene0 : MonoBehaviour {

    Vector3 FirstPoint;
    Vector3 SecondPoint;
    float xAngle;
    float yAngle;
    float xAngleTemp;
    float yAngleTemp;

    void Start()
    {
        Input.gyro.enabled = true;
        xAngle = 0;
        yAngle = 0;
        transform.rotation = Quaternion.Euler(yAngle, xAngle, 0);
    }

    void Update()
    {
        if (!Input.gyro.enabled) Input.gyro.enabled = true;

        xAngleTemp = xAngle;
        yAngleTemp = yAngle;
        xAngle = xAngleTemp + Input.gyro.rotationRateUnbiased.x;
        yAngle = yAngleTemp - Input.gyro.rotationRateUnbiased.y;
        transform.rotation = Quaternion.Euler(-xAngle, yAngle, 0.0f);

        float z = transform.eulerAngles.z;
        transform.Rotate(0, 0, -z);
    }
}
