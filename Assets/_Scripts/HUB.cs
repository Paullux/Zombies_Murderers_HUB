﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HUB : MonoBehaviour {


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Quitter()
    {
        Application.Quit();
    }

    public void OuvrirPageWeb (string PageWeb)
    {
        Application.OpenURL(PageWeb);
    }
    
    public void launchApp(string packageName)
    {
        bool fail = false;
        string bundleId = packageName;  
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

        AndroidJavaObject launchIntent = null;
        try
        {
            launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
        }
        catch (System.Exception e)
        {
            fail = true;
        }

        if (launchIntent == null) Application.OpenURL("market://details?id=" + packageName);
        
        if (fail)
        { //open app in store
            //Application.OpenURL("market://details?id=" + packageName);
        }
        else //open the app
            ca.Call("startActivity", launchIntent);

        up.Dispose();
        ca.Dispose();
        packageManager.Dispose();
        launchIntent.Dispose();
    }
}
