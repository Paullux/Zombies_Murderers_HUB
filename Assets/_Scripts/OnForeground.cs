﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnForeground : MonoBehaviour
{

    void OnApplicationFocus(bool pauseStatus)
    {
        if (pauseStatus)
        {
            int currentScene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(currentScene);
        }
    }
}